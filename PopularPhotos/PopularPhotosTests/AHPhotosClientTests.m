//
//  PopularPhotosTests.m
//  PopularPhotosTests
//
//  Created by Anthony Hoang on 8/6/15.
//  Copyright (c) 2015 Anthony Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "AHPhotosClient.h"

@interface AHPhotosClientTests : XCTestCase

@property (nonatomic, strong) AHPhotosClient *client;
@property (nonatomic, strong) XCTestExpectation *expectation;

@end

@implementation AHPhotosClientTests

- (void)setUp {
    [super setUp];
    self.client = [AHPhotosClient new];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testGetPageOfPhotos {
    self.expectation = [self expectationWithDescription:@"testGetPageOfPhotos loading..."];
    
    [[self.client getPageOfPopularPhotos:25 pageNumber:0]
     subscribeNext:^(id x) {
         XCTAssertNotNil(x);
         XCTAssertTrue([x isKindOfClass:[AHPhotoPage class]]);
         AHPhotoPage *page = x;
         
         [page.photos enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
             XCTAssertTrue([obj isKindOfClass:[AHPhoto class]]);
         }];
         
     } error:^(NSError *error) {
         XCTFail(@"testGetPageOfPhotos failed: %@", error);
         [self.expectation fulfill];
     } completed:^{
         [self.expectation fulfill];
     }];
    
    
    [self waitForExpectationsWithTimeout:5 handler:^(NSError *error) {
        
    }];
}

@end
