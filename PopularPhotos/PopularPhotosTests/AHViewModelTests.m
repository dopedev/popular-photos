//
//  AHViewModelTests.m
//  
//
//  Created by Anthony Hoang on 8/8/15.
//
//

#import <XCTest/XCTest.h>
#import "AHPhotoViewModel.h"
#import "AHPhoto.h"
#import "AHPhotoFeedViewModel.h"

@interface AHViewModelTests : XCTestCase

@property (nonatomic, strong) AHPhotoViewModel *photoViewModel;
@property (nonatomic, strong) AHPhotoFeedViewModel *feedViewModel;
@property (nonatomic, strong) XCTestExpectation *expectation;

@end

@implementation AHViewModelTests

- (void)setUp {
    [super setUp];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testPhotoViewModel {
    NSDictionary *json = @{@"comments" : @{@"count" : @(30)},
                           @"likes" : @{@"count" : @(40)},
                           @"caption" : @{@"text" : @"This is a caption"},
                           @"images" : @{@"standard_resolution" : @{@"url" : @"https://www.google.com"}}
                           };
    AHPhoto *photo = [MTLJSONAdapter modelOfClass:[AHPhoto class] fromJSONDictionary:json error:nil];
    
    self.photoViewModel = [AHPhotoViewModel viewModelWithPhoto:photo];
    
    XCTAssertNotNil(self.photoViewModel);
    XCTAssertNotNil(self.photoViewModel.name);
    XCTAssertNotNil(self.photoViewModel.caption);
    XCTAssertNotNil(self.photoViewModel.numberOfCommentsString);
    XCTAssertNotNil(self.photoViewModel.numberOfLikesString);
    
    XCTAssertTrue([self.photoViewModel.caption isEqualToString:photo.caption]);
    XCTAssertTrue([self.photoViewModel.numberOfCommentsString isEqualToString:photo.commentCount.stringValue]);
    XCTAssertTrue([self.photoViewModel.numberOfLikesString isEqualToString:photo.favoritesCount.stringValue]);
}

- (void)testFeedViewModel {
    self.feedViewModel = [AHPhotoFeedViewModel new];
    
    self.expectation = [self expectationWithDescription:@"AHViewModelTests testFeedViewModel loading..."];
    
    [[[self.feedViewModel refreshFeedCommand] execute:nil]
     subscribeError:^(NSError *error) {
         XCTFail(@"testFeedViewModel refreshFeedCommand fail: %@", error);
         [self.expectation fulfill];
     } completed:^{
         // check number of sections
         
         NSUInteger numberOfPhotos = [self.feedViewModel numberOfSections];
         XCTAssertTrue(numberOfPhotos > 0);
         
         for (int i = 0; i < numberOfPhotos; i++) {
             AHPhotoViewModel *viewModel = [self.feedViewModel viewModelForPhotoAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:i]];
             
             XCTAssertNotNil(viewModel); // only thing that's guaranteed to not be nil is the view model. its properties could be blank if it's not returned in the json
         }
         
        
         [[[self.feedViewModel nextPageCommand] execute:nil]
          subscribeError:^(NSError *error) {
              XCTFail(@"testFeedViewModel nextPageCommand fail: %@", error);
          } completed:^{
              
              NSUInteger numberOfPhotos = [self.feedViewModel numberOfSections];
              XCTAssertTrue(numberOfPhotos > 0);
              
              for (int i = 0; i < numberOfPhotos; i++) {
                  AHPhotoViewModel *viewModel = [self.feedViewModel viewModelForPhotoAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:i]];
                  
                  XCTAssertNotNil(viewModel);
              }
              
              [self.expectation fulfill];

          }];
     }];
    
    [self waitForExpectationsWithTimeout:100 handler:^(NSError *error) {
        
    }];
}

@end