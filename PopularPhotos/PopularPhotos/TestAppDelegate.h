//
//  TestAppDelegate.h
//  
//
//  Created by Anthony Hoang on 8/8/15.
//
//

#import <UIKit/UIKit.h>

/**
 *  This is the app delegate that's launched when running unit tests. This prevents unnecessary work from running through the normal workflow, and fixes an issue with ReactiveCocoa and cocoapods. (ReactiveCocoa doesn't support cocoapod installs so unit tests fail when using cocoapods for some reason...??)
 */
@interface TestAppDelegate : UIResponder

@end
