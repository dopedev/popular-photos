//
//  AHPhotosClient.m
//  
//
//  Created by Anthony Hoang on 8/6/15.
//
//

#import "AHPhotosClient.h"

//https://api.instagram.com/v1/media/popular?access_token=ACCESS-TOKEN
static NSString * const kBaseURL = @"https://api.instagram.com/v1/";
static NSString * const kClientIdParmaterKey = @"client_id";
static NSString * const kInstagramClientId = @"ffbab61db27443d190d0665c3b8b531d";

@implementation AHPhotosClient

- (instancetype)init {
    self = [self initWithBaseURL:[NSURL URLWithString:kBaseURL]];
    
    return self;
}

- (instancetype)initWithBaseURL:(NSURL *)url {
    self = [super initWithBaseURL:[NSURL URLWithString:kBaseURL]];
    
    return self;
}

- (RACSignal *)getPageOfPopularPhotos:(NSUInteger)maxSize pageNumber:(NSUInteger)page {
    static NSString *path = @"media/popular";
    
    return [[[self rac_GET:path parameters:@{ // NOTE: REMOVE HARD CODED KEYS
                                             kClientIdParmaterKey : kInstagramClientId,
                                             @"count" : @(maxSize),
                                             @"page" : @(page)
                                             }]
             reduceEach:^id(id responseObject, AFHTTPRequestOperation *operation) {
                 return responseObject;
             }] map:^AHPhotoPage *(NSDictionary *jsonDictionary) {
                 AHPhotoPage *photoPage = [MTLJSONAdapter modelOfClass:[AHPhotoPage class] fromJSONDictionary:jsonDictionary error:nil];
                 photoPage.currentPageNumber = @(page);
                 return photoPage;
             }];
}

@end
