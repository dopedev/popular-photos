//
//  AHPhotoPage.m
//
//
//  Created by Anthony Hoang on 8/7/15.
//
//

#import "AHPhotoPage.h"
#import <MTLValueTransformer.h>
#import <ReactiveCocoa.h>

@interface AHPhotoPage ()

@property (nonatomic, copy, readwrite) NSArray *photos;

@end

@implementation AHPhotoPage

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{@"photos" : @"data"
             };
}

+ (NSValueTransformer *)JSONTransformerForKey:(NSString *)key {
    
    if ([key isEqualToString:@"photos"]) {
        return [MTLValueTransformer transformerUsingForwardBlock:^NSArray *(NSArray *jsonArray, BOOL *success, NSError *__autoreleasing *error) {
            return [[[[jsonArray rac_sequence] filter:^BOOL(NSDictionary *jsonDictionary) {
                return [jsonDictionary[@"type"] isEqualToString:@"image"]; // only parse images
            }]
                    map:^AHPhoto *(NSDictionary *jsonDictionary) {
                        return [MTLJSONAdapter modelOfClass:[AHPhoto class] fromJSONDictionary:jsonDictionary error:nil];
                    }] array];
        }];
    }
    
    return nil;
}

@end
