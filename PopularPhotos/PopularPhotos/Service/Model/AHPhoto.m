//
//  AHPhoto.m
//  
//
//  Created by Anthony Hoang on 8/6/15.
//
//

#import "AHPhoto.h"
#import <MTLValueTransformer.h>

@interface AHPhoto ()

@property (nonatomic, strong, readwrite) NSNumber *commentCount;
@property (nonatomic, strong, readwrite) NSNumber *favoritesCount;
@property (nonatomic, strong, readwrite) NSNumber *viewsCount;
@property (nonatomic, strong, readwrite) NSDate *dateCreated;
@property (nonatomic, copy, readwrite) NSString *caption;
@property (nonatomic, copy, readwrite) NSString *name;
@property (nonatomic, copy, readwrite) NSURL *imageURL;

@end

@implementation AHPhoto

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"commentCount" : @"comments.count",
             @"favoritesCount" : @"likes.count",
             @"dateCreated" : @"created_time",
             @"caption" : @"caption.text",
             @"name" : @"name",
             @"imageURL" : @"images.standard_resolution.url"
             };
}

+ (NSValueTransformer *)dateCreatedJSONTransformer {
    return [MTLValueTransformer transformerUsingForwardBlock:^id(NSNumber *createdTime, BOOL *success, NSError *__autoreleasing *error) {
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:createdTime.integerValue];
        
        return date;
    }];
}

@end
