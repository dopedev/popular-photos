//
//  AHPhotoFeedViewController.h
//  
//
//  Created by Anthony Hoang on 8/7/15.
//
//

#import <UIKit/UIKit.h>
#import "AHPhotoFeedViewModelProtocol.h"

/**
 *  This is a view controller that displays a feed of photos
 */
@interface AHPhotoFeedViewController : UITableViewController

/**
 *  The view model that backs this view controller.
 */
@property (nonatomic, strong) id<AHPhotoFeedViewModelProtocol> viewModel;

@end
