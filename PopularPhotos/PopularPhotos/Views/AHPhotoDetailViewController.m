//
//  AHPhotoDetailViewController.m
//
//
//  Created by Anthony Hoang on 8/7/15.
//
//

#import "AHPhotoDetailViewController.h"
#import <Masonry.h>
#import <RACEXTScope.h>
#import <ReactiveCocoa.h>

@interface AHPhotoDetailViewController ()

@property (nonatomic, strong, readwrite) IBOutlet UIImageView *imageView;
@property (nonatomic, strong, readwrite) IBOutlet UILabel *descriptionlabel;

- (void)setupView;
- (void)setBindings;

@end

@implementation AHPhotoDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupView];
    [self setBindings];
    self.view.backgroundColor = [UIColor whiteColor];
    self.edgesForExtendedLayout = UIRectEdgeNone;
}

- (void)setupView {
    self.imageView = [UIImageView new];
    self.imageView.translatesAutoresizingMaskIntoConstraints = NO;
    self.imageView.contentMode = UIViewContentModeScaleAspectFill;
    [self.view addSubview:self.imageView];
    
    self.descriptionlabel = [UILabel new];
    self.descriptionlabel.numberOfLines = 0;
    self.descriptionlabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:self.descriptionlabel];
    
    [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.right.equalTo(self.view.mas_right);
        make.top.equalTo(self.view.mas_top).offset(10);
        make.height.equalTo(self.view.mas_height).multipliedBy(0.66);
    }];
    
    [self.descriptionlabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.imageView.mas_left).offset(10);
        make.right.equalTo(self.imageView.mas_right).offset(-10);
        make.top.equalTo(self.imageView.mas_bottom).offset(10);
        make.bottom.equalTo(self.view.mas_bottom).offset(-10);
    }];
}

- (void)setBindings {
    RAC(self, imageView.image) = [RACSignal merge:@[[RACObserve(self, viewModel.cachedPhoto) ignore:nil],
                                                    [RACObserve(self, viewModel.downloadedPhoto) ignore:nil]]]; // binds the image view to the view model
    
    RAC(self, descriptionlabel.text) = RACObserve(self, viewModel.caption); // binds the label's text to the view model
}

@end
