//
//  AHPhotoViewModel.m
//
//
//  Created by Anthony Hoang on 8/7/15.
//
//

#import "AHPhotoViewModel.h"
#import "AHPhoto.h"
#import <RACEXTScope.h>
#import <SDWebImageManager.h>

@interface AHPhotoViewModel ()

#pragma mark - Writable property redelcarations
@property (nonatomic, strong, readwrite) UIImage *downloadedPhoto;
@property (nonatomic, strong, readwrite) UIImage *cachedPhoto;
@property (nonatomic, copy, readwrite) NSString *name;
@property (nonatomic, copy, readwrite) NSString *dateCreatedString;
@property (nonatomic, copy, readwrite) NSString *caption;
@property (nonatomic, copy, readwrite) NSString *numberOfLikesString;
@property (nonatomic, copy, readwrite) NSString *numberOfCommentsString;

/**
 *  The photo object that backs this view model
 */
@property (nonatomic, strong) AHPhoto *data;

/**
 *  Sets the necessary bindings.
 */
- (void)setBindings;

@end

@implementation AHPhotoViewModel

+ (instancetype)viewModelWithPhoto:(AHPhoto *)photo {
    return [[self alloc] initWithPhoto:photo];
}

- (instancetype)initWithPhoto:(AHPhoto *)photo {
    self = [super init];
    
    if (self) {
        self.data = photo;
        [self setBindings];
    }
    
    return self;
}

- (void)setBindings {
    self.numberOfCommentsString = self.data.commentCount.stringValue;
    self.numberOfLikesString = self.data.favoritesCount.stringValue;
    self.caption = self.data.caption;
    self.dateCreatedString = @"";
    self.name = self.data.caption;
    
    @weakify(self)
    
    [[SDWebImageManager sharedManager] downloadImageWithURL:self.data.imageURL
                                                    options:0
                                                   progress:nil
                                                  completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                                      @strongify(self)
                                                      
                                                      if (image) {
                                                          
                                                          if (cacheType == SDImageCacheTypeNone) {
                                                              self.downloadedPhoto = image;
                                                          } else {
                                                              self.cachedPhoto = image;
                                                          }
                                                          
                                                      }
                                                      
                                                  }];
}

@end
