//
//  AHPhotoViewModel.h
//  
//
//  Created by Anthony Hoang on 8/7/15.
//
//

#import <Foundation/Foundation.h>
#import "AHPhotoViewModelProtocol.h"

@class AHPhoto;

/**
 *  This is a view model that backs a view that displays details about a photo. This view model is backed by a photo object from the 500px service
 */
@interface AHPhotoViewModel : NSObject <AHPhotoViewModelProtocol>

/**
 *  Factory to create a new view model. This is the reccommended method to create a new instance.
 *
 *  @param photo The AHPhoto data object that backs this view model.
 *
 *  @return The new instance.
 */
+ (instancetype)viewModelWithPhoto:(AHPhoto *)photo;

#pragma mark - AHPhotoViewModelProtocol

/**
 *  The photo the view should display. The view should bind to this property to update the view as the image is downlaoded.
 */
@property (nonatomic, strong, readonly) UIImage *downloadedPhoto;

/**
 *  The photo to display if the image was cached. The view should bind to this prooperty and display the cached photo.
 */
@property (nonatomic, strong, readonly) UIImage *cachedPhoto;

/**
 *  The view should bind to this property to display the name of the photo.
 */
@property (nonatomic, copy, readonly) NSString *name;

/**
 *  The view should bind to this property to display the date the photo was created.
 */
@property (nonatomic, copy, readonly) NSString *dateCreatedString;

/**
 *  The view should bind to this property to display the caption for the photo.
 */
@property (nonatomic, copy, readonly) NSString *caption;

/**
 *  The view should bind to this property to display the number of likes the photo received.
 */
@property (nonatomic, copy, readonly) NSString *numberOfLikesString;

/**
 *  The view should bind to this property to display the number of comments left on the photo.
 */
@property (nonatomic, copy, readonly) NSString *numberOfCommentsString;

@end
