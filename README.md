#README#

[![Build Status](https://www.bitrise.io/app/8b240b8816f288f3.svg?token=2unIAxS4_QrptuGxvUI7kg&branch=master)](https://www.bitrise.io/app/295bebc6c3631bf2)  
![alt text](https://app.ship.io/jobs/WGnCfKJ8oYdXZp_-/build_status.png "Ship.io Badge")


## Dependencies ##
This is the list of dependencies included in this project, along with why I decided to include it in the project

### [ReactiveCocoa](https://github.com/ReactiveCocoa/ReactiveCocoa) ###
I used ReactiveCocoa in this project for concurrent operations, bindings, some state management, and object mapping/transforming. This is also a tool that aids in my MVVM architecture.

###[AFNetworking (ReactiveCocoa Extensions)](https://github.com/CodaFi/AFNetworking-RACExtensions)
###
I used AFNetworking for network requests. The ReactiveCocoa extensions adds RX functionalities to the AFNetworking library.

###[DZNEmptyDataSet](https://github.com/dzenbot/dznemptydataset)###
DZNEmptyDataSet is a library that displays an empty state in a UITableView or UICollectionView.

###[Mantle](https://github.com/Mantle/Mantle)###
Mantle is used for JSON parsing and serialization.

###[Masonry](https://github.com/SnapKit/Masonry)###
I used Masonry for better AutoLayout syntax.

###[SDWebImage](https://github.com/rs/SDWebImage)###
I used SDWebImage for image downloading and caching.